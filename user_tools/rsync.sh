#!/bin/bash
######################
# MIDWAY SSH CONNECT #
######################
printUsage () {
cat << EOF

     ######################### Script $filename #########################
     To use this script either run without arguments to use the default
     behavior of syncing your midway home directory to copy to the present
     working directory or you can specify where the midway home directory
     should be copied to on your local machine.
       ./rsync.sh
     To specify the folder to copy to locally
       ./rsync.sh --transfer2    </local_folder_to_transfer_to>
     If the --help argument is passed this usage message will be displayed
       ./rsync.sh --help
     ####################################################################

EOF
}
#
# Setup
filename=`basename "$0"`
extension=${0##*.}
host=midway.rcc.uchicago.edu
user=${PWD##*/}
os_name=`uname -s`
echo "OS name is $os_name"
date=`date +%Y-%m-%d:%H:%M:%S`
#
# Verify the ssh identity file exists
if [ ! -e ./$user.key ]; then
    echo " ***************** ERROR ****************** "
    echo " SSH key identity file not found. Ensure    "
    echo " you are running this script from within    "
    echo " the rccguest folder you unzipped.          "
    echo " ****************************************** "
   exit 0
else
       command=" rsync -arv -e 'ssh -i ./$user.key' $user@$host:~/ ./"
fi
echo "------------------------------------------------------------------------------- "
echo "                            Transfer Midway home folder "
echo "       Date: $date "
echo "       Host: $host"
echo "       User: $user"
echo " "
echo "     $command"
echo "------------------------------------------------------------------------------- "
echo " "
eval $command