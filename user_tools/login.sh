#!/bin/bash
######################
# MIDWAY SSH CONNECT #
######################
printUsage () {
cat << EOF

     ######################### Script $filename #########################
     To use this script either run without arguments to use the default
     behavior of connecting to midway as a guest with X11 forwarding enabled.
       ./login.sh
     To connect without X11 forwarding pass the --noX11 flag
       ./login.sh --noX11
     If the --help argument is passed this usage message will be displayed
       ./login.sh --help
     ####################################################################

EOF
}
#
# Setup
filename=`basename "$0"`
extension=${0##*.}
host=midway.rcc.uchicago.edu
user=${PWD##*/}
os_name=`uname -s`
echo "OS name is $os_name"
date=`date +%Y-%m-%d:%H:%M:%S`
#
# Verify the ssh identity file exists
if [ ! -e ./$user.key ]; then
    echo " ***************** ERROR ****************** "
    echo " SSH key identity file not found. Ensure    "
    echo " you are running this script from within    "
    echo " the rccguest folder you unzipped.          "
    echo " ****************************************** "
   exit 0
else
#
# Check which operating system we are using
 if  [[ $os_name == "Linux" ]]; then
       command="ssh -X -i ./$user.key $user@$host"
 elif [[ $os_name == "Darwin" ]]; then
#
# Using X11 forwarding:
   if [ -z "$1" ]; then
      if hash startX 2>/dev/null ;  then
#      echo "X11 installed"
         command="ssh -Y -i ./$user.key $user@$host"
      else
         echo ""
         echo "********************** X Server is NOT INSTALLED **********************"
         echo "  If you would like to remotely display graphics and/or use a GUI"
         echo "  you need to install Apple's version of the X server called XQuartz."
         echo "  "
         read -r -p "  Do you wish to proceed without X11 forwarding enabled? [y/N] " response
         echo ""
         case "$response" in
           [yY][eE][sS]|[yY])
              command="ssh -i ./$user.key $user@$host"
             ;;
           [nN][oO]|[nN])
             exit 0
             ;;
          *)
            echo "  Response not valid. Exiting"
            echo ""
            exit 0
            ;;
         esac
      fi

   elif [[ "$1" == "--noX11" || "$1" == "-noX11" || "$1" == "--nox11" || "$1" == "-nox11" ]]; then
     command="ssh -i ./$user.key $user@$host"

   elif [[ "$1" == "--help" || "$1" == "-help" ]]; then
     printUsage
     exit 0
   else
     echo "You are not using this script as intended. Please check your usage of this script."
     echo " "
     printUsage
   fi
 else
  echo "Unrecognozied or incompatible environment. This script is not designed for your environment."
 fi
fi
echo " "
echo "------------------------------------------------------------------------------- "
echo "                            Connecting to Midway"
echo "       Date: $date "
echo "       Host: $host"
echo "       User: $user"
echo " "
echo "     $command"
echo "------------------------------------------------------------------------------- "
echo " "
eval $command