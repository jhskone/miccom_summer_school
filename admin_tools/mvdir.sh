list=`ls -al | awk -F' ' '{print $9}' | sed 's/\..*//'`

for i in $list; do
  echo $i
  mkdir $i
  mv $i.key $i
done
