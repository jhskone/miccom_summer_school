#!/opt/local/bin/expect -f

puts "\nBEGINNING INTERACTIVE RESPONSE SCRIPT\n"
#set timeout -1
set COUNT 1
puts "THE INITIAL INDEX VALUE IS $COUNT"
spawn  ./zip.sh
while { $COUNT <= 70 } {
expect "Enter password: "
send -- "M1cc0m2017\r"
sleep 1
expect "Verify password: "
send -- "PASSWORD_GOES_HERE\r"
sleep 1
set COUNT [ expr $COUNT + 1 ]
}

expect eof
