 ![ |small ](imgs/miccom.png)

## About
This repository is a collection of tools/scripts and other materials for 
setting up the resources and accounts for the [2017 MiCCOM summer school](http://miccom-center.org/summer-school-2017/). 